import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import promise from 'redux-promise';
import _ from 'lodash'; // Required for throttling searchbar if rendering videos

// Import Material-Ui Components
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import FontIcon from 'material-ui/FontIcon';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';

// Import components
import reducers from './reducers';
//import SearchBar from './components/search_bar';
import RecipeByIngredients from './components/recipeByIngredients';
import RecipeByNutrient from './components/recipeByNutrient';
import RecipeByCuisine from './components/recipeByCuisine';
import MealPlanWidget from './components/mealplanwidget';
import MealPlansIndex from './components/mealPlans';
import FoodQuestionsForm from './components/foodquestions';
import FoodTrivia from './components/foodTrivia';

<iframe
    width="350"
    height="430"
    src="https://console.dialogflow.com/api-client/demo/embedded/f3ea5ac0-bd5e-4d6e-91fe-1d9c53d169b9">
</iframe>

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { value: 1, currentPage: "form" };

    this.pageRender = this.pageRender.bind(this);
  }

  // fixed with mini hack
  pageRender = (event, target, value) => {
    this.setState((prevState, props) => {
      return { value: value }
    });
    var num = value;
    if (value == 1) {
      window.location = ""
    } else if (value == 2) {
      window.location = "/recipebynutrient"
    } else if (value == 3) {
      window.location = "/recipebyingredients"
    } else if (value == 4) {
      window.location = "/recipebycuisine"
    }
    //console.log(value);
    //console.log(this.state.value);
  }

  createMenu = () => {
    window.location = "/mealplanner"
  }

  answerQuestions = () => {
    this.setState({ currentPage: "done" })
  }

  render() {

    if (this.state.currentPage === 'form') {
      return (
        <div className="nav-bar-style">
        <MuiThemeProvider>
        <Toolbar className="toolbar">
        <ToolbarGroup>
        <RaisedButton label="Get Meal Plan" primary={true} onClick={this.createMenu} />
        </ToolbarGroup>

        <img className="logo" src="../../images/logo.png"/>

        <ToolbarGroup>
        <div>
        <SelectField value={this.state.value} onChange={this.pageRender}>
        <MenuItem value={1} primaryText="RECIPES FILTER" />
        <MenuItem value={2} primaryText="By Nutrient" />
        <MenuItem value={3} primaryText="By Ingredients" />
        <MenuItem value={4} primaryText="By Cuisine" />
        </SelectField>
        </div>
        </ToolbarGroup>
        </Toolbar>
        </MuiThemeProvider>


        <div className = "buttonAndChatbot">
        <table>
        <colgroup>
        <col span="4"/>
        </colgroup>
        <tr>
        <th>
        <MuiThemeProvider>
        <div className="questionbutton">
        <RaisedButton label="Your Food Dictionary" primary={true} onClick={this.answerQuestions} />
        </div>
        </MuiThemeProvider>
        </th>
        <th>
        <div className="chatbot-console" style={{paddingLeft: 50 + 'px'}}>
        <iframe id="chatbot" src="https://console.dialogflow.com/api-client/demo/embedded/f3ea5ac0-bd5e-4d6e-91fe-1d9c53d169b9"></iframe>
        </div>
        </th>
        </tr>
        </table>
        </div>

        </div>
        );
    } else if (this.state.currentPage === 'done') {
      return (
        <div>
        <MuiThemeProvider>
        <Toolbar className="toolbar">
        <ToolbarGroup>
        <RaisedButton label="Get Meal Plan" primary={true} onClick={this.createMenu} />
        </ToolbarGroup>

        <img className="logo" src="../../images/logo.png"/>

        <ToolbarGroup>
        <div>
        <SelectField value={this.state.value} onChange={this.pageRender}>
        <MenuItem value={1} primaryText="Recipes Filter" />
        <MenuItem value={2} primaryText="By Nutrient" />
        <MenuItem value={3} primaryText="By Ingredients" />
        <MenuItem value={4} primaryText="By Cuisine" />
        </SelectField>
        </div>
        </ToolbarGroup>
        </Toolbar>
        </MuiThemeProvider>


        <div className = "QuestionAndChatbot">
        <table>
        <colgroup>
        <col span="4"/>
        </colgroup>
        <tr>
        <th>
        <MuiThemeProvider>
        <div className="food-questions">
        <FoodQuestionsForm />
        <FoodTrivia />
        </div>
        </MuiThemeProvider>
        </th>
        <th>
        <div className="chatbot-console" style={{paddingLeft: 50 + 'px'}}>
        <iframe id="chatbot" src="https://console.dialogflow.com/api-client/demo/embedded/f3ea5ac0-bd5e-4d6e-91fe-1d9c53d169b9"></iframe>
        </div>
        </th>
        </tr>
        </table>
        </div>
        </div>
        );
    }
  }
}

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
  <BrowserRouter>
  <div>
  <Switch>
  <Route path="/generate/mealplans" component={MealPlansIndex} />
  <Route path="/mealplanner" component={MealPlanWidget} />
  <Route path="/recipebynutrient" component={RecipeByNutrient} />
  <Route path="/recipebyingredients" component={RecipeByIngredients} />
  <Route path="/recipebycuisine" component={RecipeByCuisine} />
  <Route path="/questions" component={FoodQuestionsForm} />
  <Route path="/" component={App} />
  </Switch>
  </div>
  </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));

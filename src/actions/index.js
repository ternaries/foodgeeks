import axios from 'axios';

export const FETCH_MEALPLANS = 'fetch_mealplans';

const ROOT_URL = 'https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes';
const API_KEY = '0DzTMYtS3hmshMVHxzhZM2C7bNMBp1wO53Wjsnd2lF4bNDwNwz';

export function fetchMealPlans() {
    const request = axios.get(`${ROOT_URL}/mealplans/generate`, {headers: {'X-Mashape-Key': '0DzTMYtS3hmshMVHxzhZM2C7bNMBp1wO53Wjsnd2lF4bNDwNwz'}});

    return {
        type: FETCH_MEALPLANS,
        // Middleware will help to retrieve beforehand the information when it sees payload property
        payload: request
    };
}
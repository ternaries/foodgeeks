// No need specify filename, because getting information from index.js
import { FETCH_MEALPLANS } from '../actions';
import _ from 'lodash';

// {} means an object
export default function(state = {}, action) {
    switch (action.type) {
    case FETCH_MEALPLANS:
        return _.mapKeys(action.payload.data, 'id');
        //console.log(action.payload.data); // [post1, post2]
        // {4: post} creating an object out of an array
    default:
        return state;
    }
}
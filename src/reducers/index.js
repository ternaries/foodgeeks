import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import MealPlansReducer from './reducer_mealplan';

const rootReducer = combineReducers({
  mealplans: MealPlansReducer,
  form: formReducer
});

export default rootReducer;

import React, { Component } from 'react';
import axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

class ShowRecipe extends Component {
    constructor(props) {
      super(props);
      this.state = {
        responseData: ''
      }
      axios.get("https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/" + this.props.id + "/information", {
        headers: {'X-Mashape-Key': '0DzTMYtS3hmshMVHxzhZM2C7bNMBp1wO53Wjsnd2lF4bNDwNwz'}
      }).then((response) => {
        console.log(response.data);
        this.setState({responseData: response.data});
        }).catch(function (error) {
          console.log(error);
        });
    }

    _generateRowsIngredients() {
      if(this.state.responseData != '') {
        return this.state.responseData.extendedIngredients.map(el => {
          return <TableRow>
            <TableRowColumn style={{width:70+'px'}}><img src={el.image} style={{height: 50 + 'px', width: 50 + 'px'}}/></TableRowColumn>
            <TableRowColumn>{this.toUpper(el.originalString)}</TableRowColumn>
            </TableRow>
        });
      }
    }

    _generateRowsSteps() {
      if(this.state.responseData != '') {
        return this.state.responseData.analyzedInstructions[0].steps.map(e1 => {
          return <TableRow>
            <TableRowColumn style={{width:70+'px'}}>{e1.number}</TableRowColumn>
            <TableRowColumn>{e1.step}</TableRowColumn>
            </TableRow>
        });
      }
    }

    toUpper(str) {
      return str
      .toLowerCase()
      .split(' ')
      .map(function(word) {
          return word[0].toUpperCase() + word.substr(1);
      })
      .join(' ');
    }

    render() {
      const styles = {
        headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
        },
      }

      return (
          <Card>
          <CardMedia
            overlay={
              <CardTitle title={this.state.responseData.title} subtitle={this.props.meal} />
            }>
            <img src={this.state.responseData.image} alt=""/>
          </CardMedia>
          <CardTitle title="Recipe" subtitle={"Cooking minutes: " + this.state.responseData.readyInMinutes}  />
          <CardText>
          <h2>Ingredients</h2>
          <Table>
            <TableBody displayRowCheckbox={false}>
              {this._generateRowsIngredients()}
            </TableBody>
          </Table>
          <h2 style={{paddingTop: 100 + 'px'}}> Steps </h2>
          <Table>
            <TableBody displayRowCheckbox={false}>
              {this._generateRowsSteps()}
            </TableBody>
          </Table>
          </CardText>
          </Card>
      );
    }
}

export default ShowRecipe;

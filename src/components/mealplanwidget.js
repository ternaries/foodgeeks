import React, { Component } from 'react';
import axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {Tabs, Tab} from 'material-ui/Tabs';

import ShowRecipe from './showRecipe.js'

import {
  Table,
  TableBody,
  TableHeader,
  th,
  tr,
  td,
} from 'material-ui/Table';

class MealPlanWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      diet: '',
      exclude: '',
      targetCalories: '',
      timeFrame: 'day',
      responseData: ''  //this is the data to send over
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.get('https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/mealplans/generate', {
      params: {
        diet: this.state.diet,
        exclude: this.state.exclude,
        targetCalories: this.state.targetCalories,
        timeFrame: this.state.timeFrame
      },
      headers: {'X-Mashape-Key': '0DzTMYtS3hmshMVHxzhZM2C7bNMBp1wO53Wjsnd2lF4bNDwNwz'}
    }).then((response) => {
      this.setState({responseData: response.data});
      }).catch(function (error) {
        console.log(error);
      });
  }

  handleChange(event) {
    const target = event.target;
    const name =  target.name;
    const value = target.value;

    this.setState({
      [name]: value
    });

  }

  render() {
    const style = {
      marginTop: 12,
    };
    const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
};
    if(this.state.responseData === '') {
      return (
        <MuiThemeProvider>
          <div className="mealPlanWidget">
          <h2> Generate daily meal plan!</h2>
          <form onSubmit={this.handleSubmit}>
              <TextField
                floatingLabelText="Diet"
                name="diet"
                fullWidth={true}
                id="text-field-controlled"
                hintText="Enter a diet that the meal plan has to adhere to, e.g. vegetarian, vegan, paleo etc."
                value={this.state.diet}
                onChange={this.handleChange}
              />
              <TextField
                floatingLabelText="Exclude"
                name="exclude"
                fullWidth={true}
                id="text-field-controlled"
                hintText="A comma-separated list of allergens or ingredients that must be excluded."
                value={this.state.exclude}
                onChange={this.handleChange}
              />
              <TextField
                type='number'
                floatingLabelText="Target Calories"
                name="targetCalories"
                fullWidth={true}
                id="text-field-controlled"
                hintText="What is the caloric target for one day? The meal plan generator will try to get as close as possible to that goal."
                value={this.state.targetCalories}
                onChange={this.handleChange}
              />
              <RaisedButton label="Submit" primary={true} style={style} onClick={this.handleSubmit}/>
          </form>
          </div>
        </MuiThemeProvider>
        );
      } else {
        return (
          <MuiThemeProvider>
          <div className="mealPlanWidget">
          <Tabs>
            <Tab label="Breakfast" >
              <div>
              <ShowRecipe id={this.state.responseData.meals[0].id} meal={"Breakfast"}/>
             </div>
          </Tab>
            <Tab label="Lunch" >
              <div>
              <ShowRecipe id={this.state.responseData.meals[1].id} meal={"Lunch"}/>
             </div>
          </Tab>
            <Tab label="Dinner" >
              <div>
              <ShowRecipe id={this.state.responseData.meals[2].id} meal={"Dinner"}/>
             </div>
             </Tab>
          </Tabs>
           </div>
          </MuiThemeProvider>
        )
      }
  }


}

export default MealPlanWidget;

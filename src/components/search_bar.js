import React, { Component } from 'react';

class SearchBar extends Component {
    // Initializing a state
    constructor(props) {
        super(props);
        
        // Every instance of a class has a state object
        // term gets updated as the string of user's input
        this.state = {
            selectedOption: ''
        };
    }
    chooseSearch = (selectedOption) => {
        this.setState({ selectedOption });
        this.props.onSelectedTermChange(term); // provide link to selectedTerm
        console.log(`Selected: ${selectedOption.label}`);
    }
    
    // Every render method must return some JSX
    render() {
        // Read up React Docs for event handlers available
        // Arrow functions can help clean up code quite alot
        // Always manipulate state using this.setState
        const { selectedOption } = this.state;
        const value = selectedOption && selectedOption.value;

        return (
            <div class="dropdown">
                <button onclick="chooseSearch" className="dropbtn">Recipe Selection</button>
                <div id="recipeFilter" className="dropdown-content">

        );
    }
    // Event handler
}

// Correctly export component to use in another file
export default SearchBar;
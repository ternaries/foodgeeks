import React, { Component } from 'react';
import axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

class FoodTrivia extends Component {
    constructor(props) {
      super(props);
      this.state = {value: '', responseData: ''};

      axios.get('https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/trivia/random', {
        params: {
          text: this.state.value
        },
        headers: {'X-Mashape-Key': '0DzTMYtS3hmshMVHxzhZM2C7bNMBp1wO53Wjsnd2lF4bNDwNwz'}
      }).then((response) => {
        console.log(response.data);
        this.setState({responseData: response.data.text})
        }).catch(function (error) {
          console.log(error);
        });
    }

    render() {

      return (
        <MuiThemeProvider>
          <div>
            <div style={{marginTop: '100px', marginBottom: '30px'}}>
            <h3>Random fun fact on food!</h3>
            </div>
            <p>{this.state.responseData}</p>
            </div>
        </MuiThemeProvider>
      );
    }
  }

  export default FoodTrivia;

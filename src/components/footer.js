import React, {Component} from 'react';
import Slider from 'material-ui/Slider';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import axios from 'axios';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

/**
 * The slider bar can have a set minimum and maximum, and the value can be
 * obtained through the value parameter fired on an onChange event.
 */
class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      maxCalories: 1000,
      maxCarbs: 1000,
      maxFat: 1000,
      maxProtein: 1000,
      minCalories: 0,
      minCarbs: 0,
      minFat: 0,
      minProtein: 0,
      responseData: ''

 //this is the data to send over
    };

    this.handleFirstSlider = this.handleFirstSlider.bind(this);
    this.handleSecondSlider = this.handleSecondSlider.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    //console.log('You have selected:' + this.state.maxCarbs + this.state.minCarbs + this.state.minCalories + this.state.maxCalories
     // + this.state.minFat + this.state.maxFat + this.state.minProtein + this.state.maxProtein
     // );
    event.preventDefault();
  }



  handleFirstSlider = (event, value) => {
    this.setState({maxCalories: value});
  };

  handleSecondSlider = (event, value) => {
    this.setState({maxCarbs: value});
  };


  render() {
    if(this.state.responseData === '') {
      return (
      <MuiThemeProvider>
      <div className="nutrientsComponent">
        <p>
        <label> Calories: </label>
          <span> {this.state.maxCalories}</span>
        </p>
        <Slider
          min={0}
          max={5000}
          step={100}
          value={this.state.maxCalories}
          onChange={this.handleFirstSlider}
        />

      </div>
      </MuiThemeProvider>
      );
    } 
  }
}

export default Footer;
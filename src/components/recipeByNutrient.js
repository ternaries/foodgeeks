import React, {Component} from 'react';
import Slider from 'material-ui/Slider';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import axios from 'axios';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import ShowRecipe from "./showRecipe";
/**
 * The slider bar can have a set minimum and maximum, and the value can be
 * obtained through the value parameter fired on an onChange event.
 */
class RecipeByNutrient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      maxCalories: 1000,
      maxCarbs: 1000,
      maxFat: 1000,
      maxProtein: 1000,
      minCalories: 0,
      minCarbs: 0,
      minFat: 0,
      minProtein: 0,
      responseData: '',
      currentPage: "form",
      selectedId: ""

 //this is the data to send over
    };

    this.handleFirstSlider = this.handleFirstSlider.bind(this);
    this.handleSecondSlider = this.handleSecondSlider.bind(this);
    this.handleThirdSlider = this.handleThirdSlider.bind(this);
    this.handleFourthSlider = this.handleFourthSlider.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this._onRowSelection = this._onRowSelection.bind(this);
    this._getSelectedRecipe = this._getSelectedRecipe.bind(this);
  }

  handleSubmit(event) {
    //console.log('You have selected:' + this.state.maxCarbs + this.state.minCarbs + this.state.minCalories + this.state.maxCalories
     // + this.state.minFat + this.state.maxFat + this.state.minProtein + this.state.maxProtein
     // );
    event.preventDefault();

    axios.get('https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/findByNutrients', {
      params: {
        maxCalories: this.state.maxCalories,
        maxCarbs: this.state.maxCarbs,
        maxFat: this.state.maxFat,
        maxProtein: this.state.maxProtein,
      },
      headers: {'X-Mashape-Key': '0DzTMYtS3hmshMVHxzhZM2C7bNMBp1wO53Wjsnd2lF4bNDwNwz'}
    }).then((response) => {
      //console.log(response.data);
      this.setState({responseData: response.data})
      this.setState({currentPage: "selection"})
      }).catch(function (error) {
        console.log(error);
      });
  }

  _generateRows() {
    return this.state.responseData.map(el => {
      return <TableRow>
        <TableRowColumn>{el.title}</TableRowColumn>
        </TableRow>
    })
  }


  handleFirstSlider = (event, value) => {
    this.setState({maxCalories: value});
  };

  handleSecondSlider = (event, value) => {
    this.setState({maxCarbs: value});
  };

  handleThirdSlider = (event, value) => {
    this.setState({maxFat: value});
  };

  handleFourthSlider = (event, value) => {
    this.setState({maxProtein: value});
  };

  _onRowSelection(key) {
    this.state.selectedId = this.state.responseData[key[0]].id;
    //console.log(key);
    //console.log(key[0]);
    //console.log(this.state.responseData);
    //console.log(this.state.responseData[key[0]]);
  }

  _getSelectedRecipe(event) {
    event.preventDefault();
    this.setState({ currentPage: 'done' });
  }

  render() {
    if(this.state.currentPage === "form") {
      return (
      <MuiThemeProvider>
      <div className="nutrientsComponent">
        <p>
          <label> Calories: </label>
          <span> {this.state.maxCalories}</span>
        </p>
        <Slider
          min={0}
          max={5000}
          step={100}
          value={this.state.maxCalories}
          onChange={this.handleFirstSlider}
        />

        <p>
          <label> Carbohydrate: </label>
          <span> {this.state.maxCarbs}</span>
        </p>
        <Slider
          min={0}
          max={5000}
          step={100}
          value={this.state.maxCarbs}
          onChange={this.handleSecondSlider}
        />

        <p>
          <label> Fat: </label>
          <span> {this.state.maxFat}</span>
        </p>
        <Slider
          min={0}
          max={5000}
          step={100}
          value={this.state.maxFat}
          onChange={this.handleThirdSlider}
        />

        <p>
        <label> Protein: </label>
          <span> {this.state.maxProtein}</span>
        </p>
        <Slider
          min={0}
          max={5000}
          step={100}
          value={this.state.maxProtein}
          onChange={this.handleFourthSlider}
        />
        <FlatButton id="flatbutton" label="Select" primary={true} onClick={this.handleSubmit} />
      </div>
      </MuiThemeProvider>
      );
    } else if (this.state.currentPage === 'selection') {
      return (
        <MuiThemeProvider>
        <div className="nutrientsComponent1">
        <Table onRowSelection={this._onRowSelection}>
          <TableHeader>
            <TableRow>
              <TableHeaderColumn>Meal Title</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {this._generateRows()}
          </TableBody>
        </Table>
        <RaisedButton label="Find Recipe Instructions" primary={true} onClick={this._getSelectedRecipe} />
        </div>
        </MuiThemeProvider>
      )
    } else if (this.state.currentPage === "done") {
      return (
        <MuiThemeProvider>
          <ShowRecipe id={this.state.selectedId} meal="meal" />
        </MuiThemeProvider>
      )
    }
  }
}

export default RecipeByNutrient;
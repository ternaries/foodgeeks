import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchMealPlans } from '../actions';
import _ from 'lodash';

class MealPlansIndex extends Component {
    // lifecycle method, react method
    componentDidMount() {
        // data loading process
        this.props.fetchMealPlans();
    }

    renderMealPlans() {
        return _.map(this.props.mealplan, mealplan => {
            return (
                <li className="list-group-item" key={mealplan.id}>
                    {mealplan.title}
                </li>
            );
        });
    }

    render() {
        return (
            <div>
                <div className="text-xs-right">
                    <Link classname="btn btn-primary" to="/mealplans/new">
                    Add a Meal Plan
                    </Link>
                </div>
                <h3>List of Meal Plans</h3>
                <ul className="list-group">
                    {this.renderMealPlans()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { mealplans: state.mealplans };
}

export default connect(mapStateToProps, { fetchMealPlans })(MealPlansIndex);
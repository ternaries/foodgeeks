import React, { Component } from 'react';
import axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

class FoodQuestionsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {value: '', responseData: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {

        axios.get('https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/quickAnswer', {
          params: {
            q: this.state.value
          },
          headers: {'X-Mashape-Key': '0DzTMYtS3hmshMVHxzhZM2C7bNMBp1wO53Wjsnd2lF4bNDwNwz'}
        }).then((response) => {
          console.log(response.data);
          this.setState({responseData: response.data.answer})
          }).catch(function (error) {
            console.log(error);
          });
        event.preventDefault();
  }

  render() {

    return (
      <MuiThemeProvider>
        <form onSubmit={this.handleSubmit}>
          <div style={{marginTop: '100px', marginBottom: '30px'}}>
          <h3>Ask a food question! <FontIcon className="material-icons">mood</FontIcon></h3>
          <p>We will try our best to answer based on our knowledge.</p>
          </div>
          <label>
            Question:
            <input className="inputQuestion" style={{marginLeft: '30px'}} type="text" value={this.state.value} onChange={this.handleChange} />
          </label>
          <div className="submit-button-for-food-questions">
          <RaisedButton label="Submit" primary={true} style={{marginLeft: '100px'}} type="submit" />
          </div>
          <p>{this.state.responseData}</p>
        </form>
      </MuiThemeProvider>
    );
  }
}

export default FoodQuestionsForm;
import React, {Component} from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import axios from 'axios';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import ShowRecipe from "./showRecipe";

const items = [
  <MenuItem key={1} value="african" primaryText="African" />,
  <MenuItem key={2} value="american" primaryText="American" />,
  <MenuItem key={3} value="british" primaryText="British" />,
  <MenuItem key={4} value="cajun" primaryText="Cajun" />,
  <MenuItem key={5} value="carribean" primaryText="Carribean" />,
  <MenuItem key={6} value="chinese" primaryText="Chinese" />,
  <MenuItem key={7} value="eastern european" primaryText="Eastern European" />,
  <MenuItem key={8} value="french" primaryText="French" />,
  <MenuItem key={9} value="german" primaryText="German" />,
  <MenuItem key={10} value="greek" primaryText="Greek" />,
  <MenuItem key={11} value="indian" primaryText="Indian" />,
  <MenuItem key={12} value="irish" primaryText="Irish" />,
  <MenuItem key={13} value="italian" primaryText="Italian" />,
  <MenuItem key={14} value="japanese" primaryText="Japanese" />,
  <MenuItem key={15} value="jewish" primaryText="Jewish" />,
  <MenuItem key={16} value="korean" primaryText="Korean" />,
  <MenuItem key={17} value="latin american" primaryText="Latin American" />,
  <MenuItem key={18} value="mexican" primaryText="Mexican" />,
  <MenuItem key={19} value="middle eastern" primaryText="Middle Eastern" />,
  <MenuItem key={20} value="nordic" primaryText="Nordic" />,
  <MenuItem key={21} value="southern" primaryText="Southern" />,
  <MenuItem key={22} value="spanish" primaryText="Spanish" />,
  <MenuItem key={23} value="thai" primaryText="Thai" />,
  <MenuItem key={24} value="vietnamese" primaryText="Vietnamese" />
];

/**
 * `SelectField` supports a floating label with the `floatingLabelText` property.
 * This can be fixed in place with the `floatingLabelFixed` property,
 * and can be customised with the `floatingLabelText` property.
 */
class RecipeByCuisine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 'form',
      value: '',
      responseData: '',
      selectedId: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this._onRowSelection = this._onRowSelection.bind(this);
    this._getSelectedRecipe = this._getSelectedRecipe.bind(this);
  }

  handleSubmit(event) {
    //console.log('You have selected:' + this.state.maxCarbs + this.state.minCarbs + this.state.minCalories + this.state.maxCalories
     // + this.state.minFat + this.state.maxFat + this.state.minProtein + this.state.maxProtein
     // );
    event.preventDefault();

    console.log(this.state.value);
    axios.get('https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/searchComplex', {
      params: {
        cuisine: this.state.value
      },
      headers: {'X-Mashape-Key': '0DzTMYtS3hmshMVHxzhZM2C7bNMBp1wO53Wjsnd2lF4bNDwNwz'}
    }).then((response) => {
      console.log(response.data);
      this.setState({responseData: response.data})
      this.setState({currentPage: "selection" })
      }).catch(function (error) {
        console.log(error);
      });
  }

  handleChange = (event, index, value) => this.setState({value});

  _generateRows() {
    return this.state.responseData.results.map(el => {
      return <TableRow>
        <TableRowColumn>{el.title}</TableRowColumn>
        </TableRow>
    })
  }

  _onRowSelection(key) {
    this.state.selectedId = this.state.responseData.results[key[0]].id;
    //console.log(key);
    //console.log(key[0]);
    //console.log(this.state.responseData.results[key[0]]);
  }

  _getSelectedRecipe(event) {
    event.preventDefault();
    this.setState({ currentPage: 'done' });
  }

  render() {
    if(this.state.currentPage === 'form') {
      return (
        <MuiThemeProvider>
          <div className="cuisineComponent">

          <SelectField
            value={this.state.value}
            onChange={this.handleChange}
            floatingLabelText="Your Cuisine"
            floatingLabelFixed={true}
            hintText="e.g. American">
            {items}
          </SelectField>
          <br />
          <FlatButton id="flatbutton" label="Select" primary={true} onClick={this.handleSubmit} />
          </div>
        </MuiThemeProvider>
      );
    } else if (this.state.currentPage === "selection") {
      return (
        <MuiThemeProvider>
        <div className="cuisineComponent1">
        <Table onRowSelection={this._onRowSelection}>
          <TableHeader>
            <TableRow>
              <TableHeaderColumn>Meal Title</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {this._generateRows()}
          </TableBody>
        </Table>
        <RaisedButton label="Find Recipe Instructions" primary={true} onClick={this._getSelectedRecipe} />
        </div>
        </MuiThemeProvider>
      )
    } else if (this.state.currentPage === "done") {
      return (
        <MuiThemeProvider>
          <ShowRecipe id={this.state.selectedId} meal="meal" />
        </MuiThemeProvider>
      )
    }
  }
}

export default RecipeByCuisine;
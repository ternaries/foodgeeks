import React, { Component } from 'react';
import axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import LinearProgress from 'material-ui/LinearProgress';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import ShowRecipe from './showRecipe';

class RecipeByIngredients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 'form',
      fillIngredients: true,
      ingredients: '',
      number: 10,
      ranking: 1, //this is the data to send over
      responseData: '',
      selectedId: ''
    };

    // Binding ensures you are still calling constructor properties with 'this' keyword
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this._onRowSelection = this._onRowSelection.bind(this);
    this._getSelectedRecipe = this._getSelectedRecipe.bind(this);
  }

  handleChange(event) {
    <LinearProgress mode="indeterminate" />
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    <LinearProgress mode="indeterminate" />
    //console.log('You have selected:' + this.state.fillIngredients + this.state.ingredients + this.state.number + this.state.ranking
     // + this.state.fillIngredients + this.state.ingredients + this.state.number + this.state.ranking
     // );
    event.preventDefault();

    axios.get('https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/findByIngredients', {
      params: {
        fillIngredients: this.state.fillIngredients,
        ingredients: this.state.ingredients,
        number: this.state.number,
        ranking: this.state.ranking
      },
      headers: {'X-Mashape-Key': '0DzTMYtS3hmshMVHxzhZM2C7bNMBp1wO53Wjsnd2lF4bNDwNwz'}
    }).then((response) => {
      console.log(response.data);
      this.setState({responseData: response.data});
      this.setState({currentPage: 'selection'});
      }).catch(function (error) {
        console.log(error);
      });
  }

  _generateRows() {
    return this.state.responseData.map(el => {
      return <TableRow value={el.id}>
        <TableRowColumn>{el.title}</TableRowColumn>
        <TableRowColumn>{el.missedIngredientCount}</TableRowColumn>
        <TableRowColumn>{el.likes}</TableRowColumn>
        </TableRow>
    })
  }

  _getSelectedRecipe(event) {
    event.preventDefault();
    console.log("im here");
    this.setState({ currentPage: 'done' });
  }

  _onRowSelection(key) {
    this.state.selectedId = this.state.responseData[key[0]].id;
    //console.log(key[0]);
    //console.log(this.state.responseData[key[0]]);
    //console.log(this.state.responseData[key[0]].id);
    //console.log(this.state.selectedId);
  }


  render() {
    if(this.state.currentPage === 'form') {
      return (
        <MuiThemeProvider>
        <div className="ingredientsComponent">
          <TextField
            floatingLabelText="Ingredients"
            name="ingredients"
            id="text-field-controlled"
            floatingLabelText="Your Ingredients"
            floatingLabelFixed={true}
            hintText="e.g. Chicken, Tomato"
            value={this.state.ingredients}
            onChange={this.handleChange}
            />
          <small>*Do separate your ingredients by a comma!</small>
          <br />
          <FlatButton id="flatbutton" label="Select" primary={true} onClick={this.handleSubmit} />
        </div>
        </MuiThemeProvider>
      );
    } else if (this.state.currentPage === 'selection') {
      return (
        <MuiThemeProvider>
          <div>
            <Table onRowSelection={this._onRowSelection}>
              <TableHeader>
                <TableRow>
                  <TableHeaderColumn>Meal Title</TableHeaderColumn>
                  <TableHeaderColumn>No. of Missing Ingredients</TableHeaderColumn>
                  <TableHeaderColumn>Popularity Count</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody deselectOnClickaway={false}>
                {this._generateRows()}
              </TableBody>
            </Table>
            <RaisedButton label="Find Recipe Instructions" primary={true} onClick={this._getSelectedRecipe}/>
          </div>
        </MuiThemeProvider>
      )
    } else if (this.state.currentPage === 'done') {
        return (
          <MuiThemeProvider>
            <ShowRecipe id={this.state.selectedId} meal="meal" />
          </MuiThemeProvider>
        )
    }
  }
}

export default RecipeByIngredients;
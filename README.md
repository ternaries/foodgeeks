# Hack&Roll

### Getting Started
Checkout the repo, install dependencies, then start the gulp process with the following:

```
> git clone https://TerenceLim@bitbucket.org/hnrchamps/foodgeeks.git
> cd foodgeeks
> npm install
> npm install --save lodash
> npm install material-ui
> npm install --save react-router-dom@4.0.0
> npm install --save axios redux-promise
> npm install --save redux-form
> npm start
```